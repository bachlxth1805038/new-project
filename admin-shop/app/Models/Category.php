<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models
 */
class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, Category::class, 'category_id', 'id');
    }

    /**
     * @return mixed
     */
    public function getWithPagination()
    {
        return $this->paginate(5);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function search($data)
    {
        return $this->withName($data)->when($data, function ($query) use ($data) {
            return $query->orWhere('status', (int)$data);
        })->paginate(5);
    }

    /**
     * @param $query
     * @param $name
     * @return null
     */
    public function scopeWithName($query, $name)
    {
        return $name
            ? $query->where('name', 'LIKE', '%' . $name . '%')
            : null;
    }
}
