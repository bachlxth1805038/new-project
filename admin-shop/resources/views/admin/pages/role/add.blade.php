@extends('admin.layouts.master')

@section('title')
    Add Role
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Role</h6>
        </div>
        <div class="row" style="margin: 5px">
            <form role="form" action="{{route('role.store')}}" method="post">
                <div class="col-lg-12">
                    @csrf
                    <fieldset class="form-group">
                        <label>Role name</label>
                        <input class="form-control" name="name" placeholder="Enter role name ">
                        @if($errors->has('name'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </fieldset>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="description" placeholder="Enter description role" class="form-control">
                        @if($errors->has('description'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            @foreach($permissionsParent as $permissionsParentItem)
                                <div class="card border-primary mb-3 col-md-12">
                                    <div class="card-header">
                                        <label>
                                            <input type="checkbox" value="" class="checkbox_wrapper">
                                        </label>
                                        Module {{$permissionsParentItem->name}}
                                    </div>
                                    <div class="row">
                                        @foreach($permissionsParentItem->permissionsChildrent as $permissionsChildrentItem)
                                            <div class="card-body text-primary col-md-3">
                                                <h5 class="card-title">
                                                    <label>
                                                        <input type="checkbox" name="permission_id[]"
                                                               class="checkbox_childrent"
                                                               value="{{$permissionsChildrentItem->id}}">
                                                    </label>
                                                    {{$permissionsChildrentItem->name}}
                                                </h5>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.checkbox_wrapper').on('click', function () {
            $(this).parents('.card').find('.checkbox_childrent').prop('checked',$(this).prop('checked'));
        });
    </script>
@endsection

