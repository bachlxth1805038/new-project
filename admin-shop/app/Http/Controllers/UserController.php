<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public $user;
    public $role;

    /**
     * UserController constructor.
     * @param User $user
     * @param Role $role
     */
    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $roles = $this->role->all();
        $users = $this->user->getWithPaginate();
        return view('admin.pages.user.index', compact('users', 'roles'));
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function list(Request $request)
    {
        $data = $request['data'];
        $users = $this->user->search($data);
        return view('admin.pages.user.list', compact('users'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $roles = $this->role->all();
        return view('admin.pages.user.add', compact('roles'));
    }

    /**
     * @param StoreUserRequest $request
     * @return RedirectResponse
     */
    public function store(StoreUserRequest $request)
    {
        $user = $this->user->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->newPassword),
        ]);
        $user->roles()->sync($request->role_id);
        return redirect()->route('user.index');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $user = $this->user->with('roles')->findOrFail($id);
        $roles = $this->role->all();
        return view('admin.pages.user.edit', compact('user', 'roles'));
    }

    /**
     * @param UpdateUserRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->user->findOrFail($id);
        $user->update($request->all());
        $user->roles()->sync($request->role_id);
        return response()->json(['message' => 'Successfully Edited']);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->user->destroy($id);
        return response()->json(['message' => 'Successfully Deleted']);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function search(Request $request)
    {
        $data = $request['data'];
        $users = $this->user->search($data);
        return view('admin.pages.user.list', compact('users'));
    }
}
