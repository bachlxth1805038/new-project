<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public $user;
    public $role;
    public $permission;

    /**
     * RoleController constructor.
     * @param Role $role
     * @param User $user
     * @param Permission $permission
     */
    public function __construct(Role $role, User $user, Permission $permission)
    {
        $this->user = $user;
        $this->permission = $permission;
        $this->role = $role;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $roles = $this->role->getWithPaginate();
        return view('admin.pages.role.index', compact('roles'));
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function list(Request $request)
    {
        $data = $request['data'];
        $roles = $this->role->search($data);
        return view('admin.pages.role.list', compact('roles'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $permissionsParent = $this->permission->where('parent_id', 0)->get();
        $roles = $this->role->all();
        return view('admin.pages.role.add', compact('roles', 'permissionsParent'));
    }

    /**
     * @param StoreRoleRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRoleRequest $request)
    {
        $role = $this->role->create([
            'name' => $request->name,
            'description' => $request->description,
        ]);
        $role->permissions()->sync($request->permission_id);
        return redirect()->route('role.index');
    }

    /**
     * @param StoreRoleRequest $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(StoreRoleRequest $request, $id)
    {
        $role = $this->role->findOrFail($id);
        $data = $request->all();
        ($role->update($data));
        $role->permissions()->sync($request->permission_id);
        return redirect()->route('role.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->role->destroy($id);
        return response()->json(['message' => 'Successfully Deleted']);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function search(Request $request)
    {
        $data = $request['data'];
        $roles = $this->role->search($data);
        return view('admin.pages.role.list', compact('roles'));
    }

    /**
     * @param $id
     * @return View
     */
    public function edit($id)
    {
        $permissionsParent = $this->permission->where('parent_id', 0)->get();
        $role = $this->role->findOrFail($id);
        $permissionsChecked = $role->permissions;
        return view('admin.pages.role.edit', compact('role', 'permissionsParent', 'permissionsChecked'));
    }
}
