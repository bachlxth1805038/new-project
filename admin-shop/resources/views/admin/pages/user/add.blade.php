@extends('admin.layouts.master')

@section('title')
    Create user
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">User</h6>
        </div>
        <div class="row" style="margin: 5px">
            <div class="col-lg-12">

                <form role="form" action="{{route('user.store')}}" method="post">
                    @csrf
                    <fieldset class="form-group">
                        <label>Name</label>
                        <input class="form-control" name="name" placeholder="Enter name User ">
                        @if($errors->has('name'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </fieldset>
                    <div class="form-group" >
                        <label >Email</label>
                        <input type="text" name="email"  placeholder="Enter email" class="form-control">
                        @if($errors->has('email'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </div>
                    <div class="form-group" >
                        <label>Password</label>
                        <input type="text" name="password"  placeholder="Enter password" class="form-control">
                        @if($errors->has('password'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </div>
                    <div class="form-group" >
                        <label>Role</label>
                        <select name="role_id[]" class="browser-default custom-select" multiple>
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                </form>
            </div>
        </div>
    </div>
@endsection


