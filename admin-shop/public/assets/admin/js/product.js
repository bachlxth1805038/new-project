$(document).ready(function () {
    let urlDelete;
    getList();

    function getList() {
        let url = $('#listProduct').data('action')
        callApi( url,null, GET_METHOD)
            .then((rs) => {
                $('#listProduct').replaceWith(rs);
            })
    }

    $(document).on('click', '.deleteProduct', function () {
        $('.error').hide();
        urlDelete = $(this).data('url');
    });

    $(document).on('click', '.deletePro', function () {
        callApi(urlDelete, {}, DELETE_METHOD).then((data) => {
            toastr.success(data.message, 'Notifications', {timeOut: 3000});
            getList();
            $('#delete').modal('hide');
        })
    });

    $(document).on('click', '#btn-SearchProduct', function () {
        let data = $('#txt-SearchProduct').val();
        let urlSearch = $(this).data('url');
        callApi(urlSearch, {data}, POST_METHOD)
            .then((result) => {
                console.log(data);
                $('#listProduct').replaceWith(result);
            });
    });
})

