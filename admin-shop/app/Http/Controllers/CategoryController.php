<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = $this->category->getWithPagination();
        return view('admin.pages.category.index', compact('categories'));
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function list(Request $request)
    {
        $data = $request['data'];
        $categories = $this->category->search($data);
        return view('admin.pages.category.list', compact('categories'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.pages.category.add');
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $category = $this->category->findOrFail($id);
        return response()->json(['data' => $category, 'status' => 200]);
    }

    /**
     * @param StoreCategoryRequest $request
     * @return RedirectResponse
     */
    public function store(StoreCategoryRequest $request)
    {
        $data = $request->all();
        $this->category->create($data);
        return redirect()->route('category.index')->with('message', 'Success Added');
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function edit($id)
    {
        $category = $this->category->findOrFail($id);
        return response()->json($category, 200);
    }

    /**
     * @param StoreCategoryRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(StoreCategoryRequest $request, $id)
    {
        $category = $this->category->findOrFail($id);
        $data = $request->all();
        $category->update($data);
        return response()->json(['message' => 'Success Updated']);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->category->destroy($id);
        return response()->json(['message' => 'Success Deleted']);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function search(Request $request)
    {
        $data = $request['data'];
        $categories = $this->category->search($data);
        return view('admin.pages.category.list', compact('categories'));
    }
}
