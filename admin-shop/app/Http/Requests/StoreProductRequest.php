<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'name' => 'required|min:2|max:255|',
            'description' => 'required|min:2',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'thumbnail' => 'required|image|',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute ko được để trống',
            'min' => ':attribute phải đủ từ 2-255 kí tự',
            'max' => ':attribute phải đủ từ 2-255 kí tự',
            'numeric' => ':attribute phải là 1 số',
            'thumbnail' => ':attribute ko hình ảnh',
//            'unique' => ':attribute đã tồn tại trong hệ thống',
        ];
    }

    public function attributes()
    {
        return[
            'name' => 'Tên sản phẩm',
            'description' => 'Mô tả sản phẩm',
            'quantity' => 'Số lượng sản phẩm',
            'price' => 'Đơn giá sản phẩm',
            'thumbnail' => 'Ảnh minh họa',
        ];
    }
}
