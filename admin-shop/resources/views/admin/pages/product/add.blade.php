@extends('admin.layouts.master')

@section('title')
    Add Product
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Product</h6>
        </div>
        <div class="row" style="margin: 5px">
            <div class="col-lg-12">
                <form role="form" action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <fieldset class="form-group">
                        <label>Name Product</label>
                        <input class="form-control" name="name" placeholder="Enter Name Product ">
                        @if($errors->has('name'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </fieldset>
                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input type="number" name="quantity" min="1" value="1" class="form-control">
                        @if($errors->has('quantity'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" name="price" placeholder="Enter Price Product" class="form-control">
                        @if($errors->has('price'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="promotional">Thumbnail</label>
                        <input type="file" name="thumbnail" class="form-control">
                        @if($errors->has('thumbnail'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" id="demo" cols="5" rows="5" class="form-control"></textarea>
                        @if($errors->has('description'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control cateProduct" name="category_id">
                            @foreach($category as $cate)
                                <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status">
                            <option value="1">Active</option>
                            <option value="0">Deactive</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                </form>
            </div>
        </div>
    </div>
@endsection
