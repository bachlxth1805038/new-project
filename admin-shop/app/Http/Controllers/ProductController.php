<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $product;
    protected $category;

    /**
     * ProductController constructor.
     * @param Product $product
     * @param Category $category
     */
    public function __construct(Product $product, Category $category)
    {
        $this->product = $product;
        $this->category = $category;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = $this->category->all();
        return view('admin.pages.product.index', compact('categories'));
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function list(Request $request)
    {
        $data = $request['data'];
        $products = $this->product->search($data);
        return view('admin.pages.product.list', compact('products'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $category = $this->category->getWithPagination();
        return view('admin.pages.product.add', compact('category'));
    }

    /**
     * @param StoreProductRequest $request
     * @return RedirectResponse
     */
    public function store(StoreProductRequest $request)
    {
        $this->product->store($request);
        return redirect()->route('product.index')->with('message', 'Successfully Created');
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function edit($id)
    {
        $category = $this->category->getWithPagination();
        $product = $this->product->findOrFail($id);
        return response()->json(['category' => $category, 'product' => $product]);
    }

    /**
     * @param UpdateProductRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $data = $request->all();
        $product = $this->product->findOrFail($id);
        $fileName = $this->product->updateImage($request, $product->thumbnail);
        $data['thumbnail'] = $fileName;
        $product->update($data);
        return response()->json(['message' => "Successfully Edited"]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->product->destroy($id);
        return response()->json(['message' => "Successfully deleted"]);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function search(Request $request)
    {
        $data = $request['data'];
        $products = $this->product->search($data);
        return view('admin.pages.product.list', compact( 'products'));
    }
}
