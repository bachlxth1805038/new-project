<?php

namespace App\Models;

use App\Traits\HasImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

define('IMAGE_PATH', 'img/upload/product');

class Product extends Model
{
    use HasImage;

    protected $table = "products";

    protected $fillable = [
        'name',
        'description',
        'thumbnail',
        'quantity',
        'price',
        'category_id',
        'status'
    ];

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        $file = $data->thumbnail;
        $file_name = time() . str_replace(' ', '_', $file->getClientOriginalName());
        $attributes = $data->all();
        $attributes['thumbnail'] = $file_name;
        $product = Product::create($attributes);
        $file->move('img/upload/product', $file_name);
        return $product;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function search($data)
    {
        return self::withName($data)->when($data, function ($query) use ($data) {
            return $query->orWhere('status', (int)$data);
        })->paginate(5);
    }

    /**
     * @param $query
     * @param $name
     * @return null
     */
    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', '%' . $name . '%') : null;
    }
}
