@extends('admin.layouts.master')

@section('title')
   Index Category
@endsection

@section('content')
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search formSearch" id="formSearch" >
        <div class="input-group col-md-8" style="margin-top: 10px">
            <input type="text" id="txt-Search" class="form-control bg-light border-0 abc" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button" id="btn-Search" data-url="{{route('category.search')}}">
                    <i class="fas fa-search fa-sm"></i>
                </button>
                <a href="{{route('category.create')}}">
                    <button type="button" class="btn btn-success">ADD</button>
                </a>
            </div>
        </div>
    </form>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" style="font-size: 25px">Category</h6>
        </div>
        <div id="listCategory" data-action="{{route('category.list')}}">
        </div>
    </div>

    <!-- Edit Modal-->
    <div class="modal fade editbtn" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit category <span class="title"></span></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger d-none" id="errorMsg" role="alert">
                        <ul></ul>
                    </div>
                    <div class="row" style="margin: 5px">
                        <div class="col-lg-12">
                            <form  id="form_update_category">
                                <fieldset class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" name="name" placeholder="Enter Category " id="category_name">
                                    <span class="error" style="color: red; font-size: 1rem"></span>
                                </fieldset>

                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control status" name="status" id="category_status">
                                        <option value="1" class="ht">Active</option>
                                        <option value="0" class="kht">Deactive</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success updateCategory">Save</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- delete Modal-->
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Do you want to delete ?</h5>
                    <p>If delete this category, all product of it will disappear!</p>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="margin-left: 183px;">
                    <button type="button" class="btn btn-success deleteCate">Yes</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/admin/js/category.js')}}"></script>
@endsection

