<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:2',
                'max:255',
            ],
            'email' => [
                'required',
                'min:2',
                'max:255',
                'unique:users,email,' . $this->user,
            ]
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute ko được để trống ',
            'min' => ':attribute phải đủ từ 2-255 kí tự',
            'max' => ':attribute phải đủ từ 2-255 kí tự',
            'unique' => ':attribute đã tồn tại',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Tên người dùng',
            'email' => 'Email người dùng',
        ];
    }
}
