<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $guarded = [];

    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * @return mixed
     */
    public function getWithPaginate()
    {
        return self::paginate(5);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function search($data)
    {
        return self::withName($data)->when($data, function ($query) use ($data) {
            return $query;
        })->paginate(5);
    }

    /**
     * @param $query
     * @param $name
     * @return null
     */
    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', '%' . $name . '%') : null;
    }

}
