<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::view('/', 'admin.index')->name('admin.index');

    //Category
    Route::get('category/list',[CategoryController::class, 'list'])->name('category.list');
    Route::get('category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
    Route::post('category/update/{id}', [CategoryController::class, 'update'])->name('category.update');
    Route::post('category/search', [CategoryController::class, 'search'])->name('category.search');
    Route::resource('category', CategoryController::class);

    //Product
    Route::get('product/list',[ProductController::class, 'list'])->name('product.list');
    Route::post('product/search', [ProductController::class, 'search'])->name('product.search');
    Route::get('product/getData/{id}', [ProductController::class ,'getDataById'])->name('product.getData');
    Route::resource('product', ProductController::class);

    //Role
    Route::get('roles-list', [RoleController::class, 'list'])->name('role.list');
    Route::post('role/search', [RoleController::class, 'search'])->name('role.search');
    Route::get('role/getData/{id}', [RoleController::class, 'getDataById'])->name('role.getData');
    Route::resource('role', RoleController::class);

    //User
    Route::get('user/list',[UserController::class, 'list'])->name('user.list');
    Route::post('user/search', [UserController::class, 'search'])->name('user.search');
    Route::get('user/getData/{id}', [UserController::class, 'getDataById'])->name('user.getData');
    Route::resource('user', UserController::class);
});

Route::get('admin/category', [CategoryController::class, 'index'])->name('category.index')
    ->middleware('can:category-list');
Route::get('admin/category/create', [CategoryController::class, 'create'])->name('category.create')
    ->middleware('can:category-add');
Route::get('admin/category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit')
    ->middleware('can:category-edit');
