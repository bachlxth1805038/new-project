$(document).ready(function () {
    let urlUpdate;
    let urlDelete;
    getList();

    function getList() {
        let url = $('#listCategory').data('action')
        callApi(url, null, GET_METHOD)
            .then((res) => {
                $('#listCategory').replaceWith(res);
            }).catch(function (error) {
        })
    }

    $(document).on('click', '.editCategory', function () {
        $('.error').hide();
        resetValidation();
        let url = $(this).data('url');
        urlUpdate = $(this).data('action');
        callApi(url, {}, GET_METHOD)
            .then((res) => {
                $('#category_name').val(res.data.name);
            });
    });

    $(document).on('click', '.updateCategory', function () {
        let data = {
            name: $('#category_name').val(),
            status: $('#category_status').val(),
        }
        callApi(urlUpdate, data, PUT_METHOD)
            .then((res) => {
                if (res.exist) {
                    alert('Category was exist');
                    return false;
                }
                getList();
                toastr.success(res.message, 'Notifications', {timeOut: 3000})
                $('#edit').modal('hide');
            }).catch((res) => {
            showError(res);
        });
    });

    function showError(res) {
        $('#errorMsg').removeClass('d-none');
        $.each(res.responseJSON.errors, function (key, value) {
            $('body').find('#errorMsg').html('<li>' + value + '</li>');
        })
    }

    $(document).on('click', '.deleteCategory', function () {
        $('.error').hide();
        urlDelete = $(this).data('url');
    });

    $(document).on('click', '.deleteCate', function () {
        callApi(urlDelete, {}, DELETE_METHOD).then((res) => {
            toastr.success(res.message, 'Notifications', {timeOut: 3000});
            getList();
            $('#delete').modal('hide');
        })
    });

    $(document).on('click', '#btn-Search', function () {
        let data = $('#txt-Search').val();
        let urlSearch = $(this).data('url')
        callApi(urlSearch, {data}, POST_METHOD)
            .then((rs) => {
                $('#listCategory').replaceWith(rs);
            });
    });

    function resetValidation() {
        $('#errorMsg').addClass('d-none');
    }
})
